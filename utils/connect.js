//Requerimos el módulo de mongoose que hemos instalado antes
const mongoose = require("mongoose");

//Almacenamos la URL de Mongo Atlas con la contraseña buena
const DB_URL =
"mongodb+srv://root:Suv1zgTDGCP3ycCt@clusternodeproject.wqbnxph.mongodb.net/?retryWrites=true&w=majority";

//Creamos una función "connect" para conectarnos con la base de datos gracias a mongoose
const connect = () => {
  mongoose.connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
};

//Exportamos la función "connect"
module.exports = connect;