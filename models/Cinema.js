//Requerimos mongoose
const mongoose = require("mongoose");

//Inicializamos la variable schema
const Schema = mongoose.Schema;

//Creamos el schema de cinema con los parámetros con el tipo que queremos y decidiendo si queremos que sean obligatorios, además, añadimos que se genere un nuevo apartado con cuándo se ha creado la entrada.
const cinemaSchema = new Schema(
  {
    name: { type: String, required: true },
    location: { type: String, required: true },
    movies: [{ type: mongoose.Types.ObjectId, ref: "Movie" }]
  },
  {
    timestamps: true,
  }
);

//Creamos la variable Cinema utilizando mongoose para poder exportarla después
const Cinema = mongoose.model("Cinema", cinemaSchema);

//Exportamos el modelo Cinema
module.exports = Cinema;