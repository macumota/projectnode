//Requerimos mongoose
const mongoose = require("mongoose");

//Inicializamos la variable schema
const Schema = mongoose.Schema;

//Creamos el schema de movie con los parámetros con el tipo que queremos y decidiendo si queremos que sean obligatorios, además, añadimos que se genere un nuevo apartado con cuándo se ha creado la entrada.
const movieSchema = new Schema(
  {
    title: { type: String, required: true },
    director: { type: String, required: true },
    year: { type: Number },
    genre: { type: String, required: true },
  },
  {
    timestamps: true,
  }
);

//Creamos la variable Movie utilizando mongoose para poder exportarla después
const Movie = mongoose.model("Movie", movieSchema);

//Exportamos el modelo Movie
module.exports = Movie;