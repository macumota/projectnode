//Requerimos mongoose y Movie
const mongoose = require("mongoose");
const Movie = require("../models/Movie.js");

//Creamos la variable movie con el array de objetos con la información semilla de las películas.
const movies = [
  {
    title: "The Matrix",
    director: "Hermanas Wachowski",
    year: 1999,
    genre: "Acción",
  },
  {
    title: "The Matrix Reloaded",
    director: "Hermanas Wachowski",
    year: 2003,
    genre: "Acción",
  },
  {
    title: "Buscando a Nemo",
    director: "Andrew Stanton",
    year: 2003,
    genre: "Animación",
  },
  {
    title: "Buscando a Dory",
    director: "Andrew Stanton",
    year: 2016,
    genre: "Animación",
  },
  {
    title: "Interestelar",
    director: "Christopher Nolan",
    year: 2014,
    genre: "Ciencia ficción",
  },
  {
    title: "50 primeras citas",
    director: "Peter Segal",
    year: 2004,
    genre: "Comedia romántica",
  },
];

//Guardamos la URL de la base de datos de Atlas en una variable
const DB_URL = "mongodb+srv://root:Suv1zgTDGCP3ycCt@clusternodeproject.wqbnxph.mongodb.net/?retryWrites=true&w=majority";

//Creamos una variable movieDocuments y mapeamos movies. Conectamos, utilizando mongoose y, entonces, buscamos todas las películas y las dropeamos. Si todo está bien, las añadimos utilizando la variable movieDocuments, mediante la cual todas se organizan con el schema de Movie. Y, finalmente, nos desconectamos de mongoose.
const movieDocuments = movies.map((movie) => new Movie(movie));
mongoose
  .connect(DB_URL,{
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(async () => {
    const allMovies = await Movie.find();
    if (allMovies.length)
    {
      await Movie.collection.drop();
    }
  })
  .catch(err)
  .then(async () =>
  {
    await Movie.insertMany(movieDocuments);
  })
  .catch(err)
  .finally(() => mongoose.disconnect());