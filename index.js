//Requerimos los módulos instalados con el package.json y que queremos utilizar
const express = require("express");
const cors = require("cors");

//Requerimos las funciones que necesitamos
const connect = require("./utils/connect.js");

//Requerimos los router necesarios
const moviesRouter = require("./routes/movies.routes.js");
const cinemasRouter = require("./routes/cinemas.routes.js");

//Ejecutamos connect para conectarnos a la base de datos. (Aquí tuve un problema porque al ejecutar connect, me salía un error en el servidor escuchando y era, aún no sé por qué, por la autenticación en Atlas, por lo que cambié la contraseña al usuario root y ya se solucionó el problema)
connect();

//Indicamos que el puerto para nuestro servidor será el 3000.
const PORT = 4000;

//Creamos el servidor
const server = express();

//Creamos una constante router y ejecutamos Router, de express
const router = express.Router();

//Body parser para evitar que nos dé error al intentar probar con Postman
server.use(express.json());
server.use(express.urlencoded({ extended: false }));

//Requerimos el documento Movie que tiene el esquema de la colección Movie
const Movie = require("./models/Movie");
const Cinema = require("./models/Cinema");

//Ejecutamos connect para conectarnos a la base de datos.
server.use(cors());

//Ejecutamos el servidor con un slash, que será el punto de inicio y utilizaremos express.Router.
server.use("/", moviesRouter);
server.use("/", cinemasRouter);

//Hacemos que el servidor se quede escuchando todo lo que ocurra.
server.listen(PORT, () => {});