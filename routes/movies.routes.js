//Requerimos express y el modelo de movie
const express = require("express");
const Movie = require("../models/Movie.js");

//Inicializamos express.Router como moviesRouter
const moviesRouter = express.Router();

//Creamos el endpoint para conseguir todas las películas. Es asíncrona para poder esperar que encuentre el total de películas antes de devolverlas. Y después, poenmos un 404 por si hubiera algún error (se podría poner cualquier código de error que creamos conveniente)
moviesRouter.get("/movies", async (req, res) => {
  try {
    const movies = await Movie.find();
    return res.status(200).json(movies);
  } catch (err) {
    return res.status(404).json(err);
  }
});

//Creamos el endpoint para conseguir la película según el id de la misma. Creamos una variable id que se identifique con la query del usuario, entonces, se buscará la película con ese id y se devolverá el .json de la misma. Si hay algún error, devolverá 404. (Encontré un error porque inicialmente en la ruta sólo puse "/movies/:id" y en este paso no hubo problema, pero al utilizar el get de los títulos, el siguiente, me aparecía la respuesta del título correcta y una segunda respuesta de un id incorrecto. Entonces, me di cuenta de que necesitaba poner el tipo de parámetro y el parámetro en sí después, para que no se repitiera en la búsqueda con otros parámetros).
moviesRouter.get("/movies/id/:id", async (req, res) => {
  const id = req.params.id;
  try {
    const movieById = await Movie.findById(id);
    if (id.length > 0) {
      return res.status(200).json(movieById);
    } else {
      res.status(404).json("No se encuentra la película en la base de datos.");
    }
  } catch (err) {
    return res.status(500).json(err);
  }
});

//Creamos el endpoint para conseguir la película según el título de la misma. Creamos una variable título y se buscará la película con el título que contenga el valor de título, sin importar si hay algo delante o detrás (asteriscos) o si es en mayúsculas o minúsculas(opción: i) y se devolverá el .json de la misma. Si hay algún error, devolverá 404.
moviesRouter.get("/movies/title/:title", async (req, res) => {
  const { title } = req.params;
  try {
    const movieByTitle = await Movie.find({
      title: { $regex: ".*" + title + ".*", $options: "i" },
    });
    if (movieByTitle.length > 0) {
      return res.status(200).json(movieByTitle);
    } else {
      res
        .status(404)
        .json(
          "No existen películas que contengan esa palabra en su título en la base de datos."
        );
    }
  } catch (err) {
    return res.status(500).json(err);
  }
});

//Creamos el endpoint para conseguir las películas según el género de las  mismas. Creamos una variable género que contenga (da igual si en mayúsculas o minúsculas o con algo delante o detrás) algún término que se identifique con la query del usuario, entonces, se buscará la película con ese término en el género y se devolverá el .json de la misma. Si hay algún error, devolverá 404.
moviesRouter.get("/movies/genre/:genre", async (req, res) => {
  const { genre } = req.params;
  try {
    const movieByGenre = await Movie.find({
      genre: { $regex: ".*" + genre + ".*", $options: "i" },
    });
    return res.status(200).json(movieByGenre);
  } catch (err) {
    return res
      .status(404)
      .json("No existen películas de ese género en la base de datos.");
  }
});

//Creamos el endpoint para conseguir las películas estrenadas de 2010 en adelante. Creamos una variable year que contenga la cifra que introduzca el usuario con la query, entonces, se buscará la película o películas que se hayan estrenado ese año o posteriormermente (gracias a $gt) y se devolverá el .json de la misma o mismas ordenado de menor a mayor. Si hay algún error, devolverá 404.
moviesRouter.get("/movies/year/:year", async (req, res) => {
  const { year } = req.params;
  try {
    const movieByYear = await Movie.find({ year: { $gt: year } });
    return res.status(200).json(movieByYear.sort().reverse());
  } catch (err) {
    return res
      .status(404)
      .json(
        "No existen películas estrenadas en el año que ha introducido o posteriormente en la base de datos."
      );
  }
});

//Creamos el endpoint para generar una nueva entrada en la base de datos de movies. Creamos una variable newmovie que cree con una neuva película tomando como referencia el Schema Movie, lo salvamos después y lo devolvemos creado en formato de json.
moviesRouter.post("/movies", async (req, res, next) => {
  try {
    const newMovie = new Movie({
      title: req.body.title,
      director: req.body.director,
      year: req.body.year,
      genre: req.body.genre,
    });
    const createdMovie = await newMovie.save();
    return res.status(200).json(createdMovie);
  } catch (err) {
    next(err);
  }
});

//Creamos el endpoint para modificar las películas. Guardamos la id de la película sin modificar, creamos una variable con la película modificada, cambiamos el id de la película modificada por el id original, buscamos la original con el id y cambiamos la información por la película modificada. Después devolvemos la película modificada. Según el ejemplo, se devolvía updatedMovie, pero de la forma en la que lo he puesto, es una devolución directa.
moviesRouter.put("/movies/:id", async (req, res, next) => {
	try {
	  const {id} = req.params;
	  const modifiedMovie = new Movie(req.body);
	  modifiedMovie._id=id;
	  await Movie.findByIdAndUpdate(id, modifiedMovie);
	  return res.status(200).json(modifiedMovie);
	} catch (err) {
	  next(err);
	}
  });

  //Creamos el endpoint para eliminar alguna película. Guardamos el id de la película para poder buscarla después y la eliminamos.
  moviesRouter.delete("/movies/:id", async (req, res, next) => {
    try {
        const {id} = req.params;
        const movie = await Movie.findByIdAndDelete(id);
        return res.status(200).json(`La película ${movie.title} ha sido eliminada.`);
    } catch (error) {
        return next(error);
    }
});

//Exportamos el router completo
module.exports = moviesRouter;
