//Requerimos express y el modelo de cinema
const express = require("express");
const Cinema = require("../models/Cinema.js");

//Inicializamos express.Router como cinemasRouter
const cinemasRouter = express.Router();

//Creamos el endpoint para conseguir todos los cines, con movies completado y que devuelva después la variable cinemas.
cinemasRouter.get("/cinemas", async (req, res) => {
  try {
    const cinemas = await Cinema.find().populate("movies");
    return res.status(200).json(cinemas);
  } catch (err) {
    return res.status(404).json(err);
  }
});

//Creamos el endpoint para conseguir el cine según el id del mismo. Creamos una variable id que se identifique con la query del usuario, entonces, se buscará la película con ese id y se devolverá el .json de la misma. Si hay algún error, devolverá 404.
cinemasRouter.get("/cinemas/id/:id", async (req, res) => {
  const id = req.params.id;
  try {
    const cinemaById = await Cinema.findById(id).populate('movies');
    if (id.length > 0) {
      return res.status(200).json(cinemaById);
    } else {
      res.status(404).json("No se encuentra el cine en la base de datos.");
    }
  } catch (err) {
    return res.status(500).json(err);
  }
});

//Creamos el endpoint para conseguir el cine según el nombre del mismo. Creamos una variable nombre y se buscará el cine con el nombfre que contenga el valor de name, sin importar si hay algo delante o detrás (asteriscos) o si es en mayúsculas o minúsculas(opción: i) y se devolverá el .json de la misma. Si hay algún error, devolverá 404.
cinemasRouter.get("/cinemas/name/:name", async (req, res) => {
  const { name } = req.params;
  try {
    const cinemaByName = await Cinema.find({
      name: { $regex: ".*" + name + ".*", $options: "i" },
    }).populate('movies');
    return res.status(200).json(cinemaByName);
  } catch (err) {
    res
      .status(404)
      .json(
        "No existen cines que se llamen de esa manera en la base de datos."
      );
  }
});

//Creamos el endpoint para conseguir los cines según la localización de los mismos. Creamos una variable location que contenga (cambiado todo a minúsculas) algún término que se identifique con la query del usuario, entonces, se buscará el cine con ese término en la localización y se devolverá el .json de la misma. Si hay algún error, devolverá 404.
cinemasRouter.get("/cinemas/location/:location", async (req, res) => {
  const { location } = req.params;
  try {
    const cinemaByLocation = await Cinema.find({
      location: { $regex: ".*" + location + ".*", $options: "i" },
    }).populate('movies');
    return res.status(200).json(cinemaByLocation);
  } catch (err) {
    return res
      .status(404)
      .json("No existen cines en esa localización en la base de datos.");
  }
});

//Creamos el endpoint para generar una nueva entrada en la base de datos de cinemas. Creamos una variable newcinema que cree con un nuevo cine tomando como referencia el Schema Cinema, lo salvamos después y lo devolvemos creado en formato de json.
cinemasRouter.post("/cinemas", async (req, res, next) => {
  try {
    const newCinema = new Cinema({
      name: req.body.name,
      location: req.body.location,
      movies: [],
    });
    const createdCinema = await newCinema.save();
    return res.status(200).json(createdCinema);
  } catch (err) {
    next(err);
  }
});

//Creamos el endpoint para modificar los cines. Guardamos la id del cine sin modificar, creamos una variable con el cine modificado, cambiamos el id del cine modificado por id original, buscamos el original con el id y cambiamos la información por el cine modificado. Después devolvemos el cine modificado. Según el ejemplo, se devolvía updatedCinema, pero de la forma en la que lo he puesto, es una devolución directa.
cinemasRouter.put("/cinemas/:id", async (req, res, next) => {
  try {
    const { id } = req.params;
    const modifiedCinema = new Cinema(req.body);
    modifiedCinema._id = id;
    await Cinema.findByIdAndUpdate(id, modifiedCinema);
    return res.status(200).json(modifiedCinema);
  } catch (err) {
    next(err);
  }
});

//Creamos el endpoint para añadir películas a los cines. Guardamos la id del cine y la de la película, creamos una variable con el cine modificado y lo identificamos con el id, al que le pusheamos la nueva película. Después devolvemos el cine modificado. 
cinemasRouter.put("/add-movies", async (req, res, next) => {
  try {
      const { cinemaId, movieId } = req.body;
      const updatedCinema = await Cinema.findByIdAndUpdate(
          cinemaId,
          { $push: { movies: movieId } },
          { new: true }
      );
      return res.status(200).json(updatedCinema);
  } catch (error) {
      return next(error);
  }
});

//Creamos el endpoint para eliminar algún cine. Guardamos el id del cine para poder buscarlo después y lo eliminamos.
cinemasRouter.delete("/cinemas/:id", async (req, res, next) => {
  try {
    const { id } = req.params;
    const cinema = await Cinema.findByIdAndDelete(id);
    return res.status(200).json(`El cine ${cinema.name} ha sido eliminado.`);
  } catch (error) {
    return next(error);
  }
});

//Exportamos el router completo
module.exports = cinemasRouter;
